#include "stdafx.h"
#include "GUI_Slider.h"
#include "LoadSpriteManager.h"


GUI_Slider::GUI_Slider(sf::Vector2f p_Position, sf::Sprite* p_pSprite, sf::Vector2f p_SliderOffset,
	sf::Vector2f p_SliderDimension, sf::Color p_SliderColor, float p_MinValue, float p_MaxValue, float p_StartValue, LoadSpriteManager* p_pSpriteManager)
{
	m_SliderRenderData.m_pBackgroundSprite = p_pSprite;
	m_SliderRenderData.m_pBackgroundSprite->setPosition(p_Position);

	m_OriginalRectSize = p_SliderDimension;
	sf::RectangleShape* rect = new RectangleShape(m_OriginalRectSize);
	rect->setPosition(p_Position + p_SliderOffset);
	rect->setFillColor(p_SliderColor);
	m_SliderRenderData.m_pSliderRectangle = rect;

	m_MinValue = p_MinValue;
	m_MaxValue = p_MaxValue;
	m_CurrentValue = MathEx::S_ClampValue(p_StartValue, p_MinValue, p_MaxValue);

	m_pSpriteManager = p_pSpriteManager;
}


GUI_Slider::~GUI_Slider()
{
	m_pSpriteManager->DeleteSprite(m_SliderRenderData.m_pBackgroundSprite);
	m_SliderRenderData.m_pBackgroundSprite = nullptr;
	delete m_SliderRenderData.m_pSliderRectangle;
	m_SliderRenderData.m_pSliderRectangle = nullptr;
}


void GUI_Slider::OnValueChange(float p_Value)
{
	float value = MathEx::S_ClampValue(p_Value, m_MinValue, m_MaxValue);
	float percent = (value - m_MinValue) / (m_MaxValue - m_MinValue);

	sf::Vector2f newSize = sf::Vector2f(m_OriginalRectSize.x * percent, m_OriginalRectSize.y);
	m_SliderRenderData.m_pSliderRectangle->setSize(newSize);
}


GUI_Type GUI_Slider::GetGUIType()
{
	return GUI_Type::GUI_Type_Slider;
}


SliderRenderData* GUI_Slider::GetSliderRenderData()
{
	return &m_SliderRenderData;
}


sf::Vector2f GUI_Slider::GetPosition()
{
	return m_SliderRenderData.m_pBackgroundSprite->getPosition();
}

