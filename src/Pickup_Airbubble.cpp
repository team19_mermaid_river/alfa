#include "stdafx.h"
#include "Pickup_Airbubble.h"
#include "LoadSpriteManager.h"
#include "Collider_Circle.h"


Pickup_Airbubble::Pickup_Airbubble(sf::Vector2f p_Position, int p_AirAmount, float p_RiseSpeed, LoadSpriteManager* p_pSpriteManager)
{
	m_pSpriteMananger = p_pSpriteManager;
	m_pSprite = m_pSpriteMananger->CreateSprite("../assets/pickup_bubble.png", sf::Vector2i(0, 0), sf::Vector2i(1476, 1476));
	m_pSprite->setOrigin(sf::Vector2f(m_pSprite->getLocalBounds().width * 0.5f, m_pSprite->getLocalBounds().height * 0.5f));
	m_pSprite->setPosition(p_Position);
	m_pSprite->setScale(sf::Vector2f(1.0f - m_WobbleAmount, 1.0f));

	Collider_Circle* collider = new Collider_Circle(this);
	m_pCollider = dynamic_cast<Base_Collider*>(collider);
	m_pCollider->UpdateCollider();

	m_AirAmount = p_AirAmount;
	m_RiseSpeed = p_RiseSpeed;
}


Pickup_Airbubble::~Pickup_Airbubble()
{
	m_pSpriteMananger->DeleteSprite(m_pSprite);
	m_pSprite = nullptr;
	delete m_pCollider;
	m_pCollider = nullptr;
}


void Pickup_Airbubble::Update(float p_DeltaTime)
{
	m_pSprite->setScale(Wobble(p_DeltaTime));

	sf::Vector2f postion = m_pSprite->getPosition();
	m_Position.y += m_RiseSpeed * p_DeltaTime;
	m_pSprite->setPosition(postion);

	m_pCollider->UpdateCollider();
}


sf::Vector2f Pickup_Airbubble::Wobble(float p_DeltaTime)
{
	sf::Vector2f curScale = m_pSprite->getScale();

	if (m_WobbleAxis == WobbleAxis::WobbleAxis_X)
	{
		if (curScale.x > (1.0f - m_WobbleAmount) && curScale.y < 1.0f)
		{
			curScale.x -= m_WobbleSpeed * p_DeltaTime;
			curScale.y += m_WobbleSpeed * p_DeltaTime;

			curScale.x = MathEx::S_ClampValue(curScale.x, 1.0f - m_WobbleAmount, 1.0f);
			curScale.y = MathEx::S_ClampValue(curScale.y, 1.0f - m_WobbleAmount, 1.0f);
		}
		else if (curScale.x == (1.0f - m_WobbleAmount) && curScale.y == 1.0f)
		{
			m_WobbleAxis = WobbleAxis::WobbleAxis_Y;
		}
	}
	else if (m_WobbleAxis == WobbleAxis::WobbleAxis_Y)
	{
		if (curScale.y > (1.0f - m_WobbleAmount) && curScale.x < 1.0f)
		{
			curScale.x += m_WobbleSpeed * p_DeltaTime;
			curScale.y -= m_WobbleSpeed * p_DeltaTime;

			curScale.x = MathEx::S_ClampValue(curScale.x, 1.0f - m_WobbleAmount, 1.0f);
			curScale.y = MathEx::S_ClampValue(curScale.y, 1.0f - m_WobbleAmount, 1.0f);
		}
		else if (curScale.x == (1.0f - m_WobbleAmount) && curScale.y == 1.0f)
		{
			m_WobbleAxis = WobbleAxis::WobbleAxis_X;
		}
	}
	return curScale;
}


Base_Collider * Pickup_Airbubble::GetCollider()
{
	return m_pCollider;
}


PickupType Pickup_Airbubble::GetPickupType()
{
	return PickupType::PickupType_Airbubble;
}


sf::Sprite * Pickup_Airbubble::GetSprite()
{
	return m_pSprite;
}


sf::Vector2f Pickup_Airbubble::GetPosition()
{
	return m_pSprite->getPosition();
}


int Pickup_Airbubble::GetAirAmount()
{
	return m_AirAmount;
}
