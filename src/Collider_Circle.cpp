#include "stdafx.h"
#include "Collider_Circle.h"
#include "PlayerEntity.h"
#include "Base_Pickup.h"
#include "Base_Entity.h"
#include "Base_Projectile.h"
#include "Base_GUI.h"


Collider_Circle::Collider_Circle(PlayerEntity* p_pParent)
{
	m_pPlayerParent = p_pParent;
	m_ParentType = ParentType::ParentType_Player;
}


Collider_Circle::Collider_Circle(Base_GUI* p_pParent)
{
	m_pGUIParent = p_pParent;
	m_ParentType = ParentType::ParentType_GUI;
}


Collider_Circle::Collider_Circle(Base_Entity* p_pParent)
{
	m_pEntityParent = p_pParent;
	m_ParentType = ParentType::ParentType_Entity;
}


Collider_Circle::Collider_Circle(Base_Pickup* p_pParent)
{
	m_pPickupParent = p_pParent;
	m_ParentType = ParentType::ParentType_Pickup;
}


Collider_Circle::Collider_Circle(Base_Projectile* p_pParent)
{
	m_pProjectileParent = p_pParent;
	m_ParentType = ParentType::ParentType_Projectile;
}


Collider_Circle::~Collider_Circle()
{

}


void Collider_Circle::UpdateCollider()
{
	if (m_pEntityParent == nullptr && m_pPickupParent == nullptr && m_pPlayerParent == nullptr
		&& m_pProjectileParent == nullptr)
	{
		return;
	}

	switch (m_ParentType)
	{
	case ParentType::ParentType_Entity:
		if (m_pEntityParent != nullptr)
		{
			m_Radius = SetRadius(m_pEntityParent->GetSprite());
			m_Position = m_pEntityParent->GetSprite()->getPosition();
		}
		break;
	case ParentType::ParentType_Pickup:
		if (m_pPickupParent != nullptr)
		{
			m_Radius = SetRadius(m_pPickupParent->GetSprite());
			m_Position = m_pPickupParent->GetSprite()->getPosition();
		}
		break;
	case ParentType::ParentType_Player:
		if (m_pPlayerParent != nullptr)
		{
			m_Radius = SetRadius(m_pPlayerParent->GetSprite());
			m_Position = m_pPlayerParent->GetSprite()->getPosition();
		}
		break;
	case ParentType::ParentType_Projectile:
		if (m_pProjectileParent != nullptr)
		{
			m_Radius = SetRadius(m_pProjectileParent->GetSprite());
			m_Position = m_pProjectileParent->GetSprite()->getPosition();
		}
		break;
	case ParentType::ParentType_GUI:
		if (m_pGUIParent != nullptr)
		{
			m_Radius = SetRadius(m_pGUIParent->GetSprite());
			m_Position = m_pGUIParent->GetSprite()->getPosition();
		}
		break;
	}
}


float Collider_Circle::SetRadius(sf::Sprite* p_pSprite)
{
	if (p_pSprite->getGlobalBounds().height > p_pSprite->getGlobalBounds().width)
	{
		return p_pSprite->getGlobalBounds().height;
	}
	return p_pSprite->getGlobalBounds().width;
}


sf::Vector2f Collider_Circle::GetPosition()
{
	return m_Position;
}


float Collider_Circle::GetRadius()
{
	return m_Radius;
}


ColliderType Collider_Circle::GetColliderType()
{
	return ColliderType::ColliderType_Circle;
}

