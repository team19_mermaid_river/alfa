#include "stdafx.h"
#include "AudioManager.h"
#include "Audio_Clip.h"
#include "Audio_Music.h"


AudioManager::AudioManager()
{

}


AudioManager::~AudioManager()
{
	{
		auto it = m_mpSoundBuffers.begin();

		while (it != m_mpSoundBuffers.end())
		{
			delete it->second;
			it++;
		}
		m_mpSoundBuffers.clear();
	}
	{
		auto it = m_vpAudioClips.begin();

		while (it != m_vpAudioClips.end())
		{
			delete *it;
			it++;
		}
		m_vpAudioClips.clear();
	}
	{
		auto it = m_vpAudioMusic.begin();

		while (it != m_vpAudioMusic.end())
		{
			delete *it;
			it++;
		}
		m_vpAudioMusic.clear();
	}
}


Audio_Clip* AudioManager::CreateAudioClip(const std::string p_Filepath)
{
	auto it = m_mpSoundBuffers.find(p_Filepath);

	if (it == m_mpSoundBuffers.end())
	{
		sf::SoundBuffer* buffer = new SoundBuffer();
		buffer->loadFromFile(p_Filepath);

		m_mpSoundBuffers.insert(std::pair <std::string, sf::SoundBuffer*>(p_Filepath, buffer));
		it = m_mpSoundBuffers.find(p_Filepath);
	}

	Audio_Clip* clip = new Audio_Clip(new sf::Sound(*it->second));
	m_vpAudioClips.push_back(clip);

	return clip;
}


Audio_Clip* AudioManager::CreateAudioClip(const std::string p_Filepath, float p_Volume, float p_Pitch)
{
	auto it = m_mpSoundBuffers.find(p_Filepath);

	if (it == m_mpSoundBuffers.end())
	{
		sf::SoundBuffer* buffer = new SoundBuffer();
		buffer->loadFromFile(p_Filepath);

		m_mpSoundBuffers.insert(std::pair <std::string, sf::SoundBuffer*>(p_Filepath, buffer));
		it = m_mpSoundBuffers.find(p_Filepath);
	}

	Audio_Clip* clip = new Audio_Clip(new sf::Sound(*it->second));
	clip->SetPitch(p_Pitch);
	clip->SetVolume(p_Volume);
	m_vpAudioClips.push_back(clip);

	return clip;
}


Audio_Music* AudioManager::CreateAudioMusic(const std::string p_Filepath)
{
	sf::Music* source = new sf::Music();
	source->openFromFile(p_Filepath);

	Audio_Music* musicClip = new Audio_Music(source);
	m_vpAudioMusic.push_back(musicClip);

	return musicClip;
}


Audio_Music* AudioManager::CreateAudioMusic(const std::string p_Filepath, float p_Volume, float p_Pitch)
{
	sf::Music* source = new sf::Music();
	source->openFromFile(p_Filepath);

	source->setVolume(p_Volume);
	source->setPitch(p_Pitch);

	Audio_Music* musicClip = new Audio_Music(source);
	m_vpAudioMusic.push_back(musicClip);

	return musicClip;
}


void AudioManager::DeleteAudioClip(Audio_Clip* p_pAudioClip)
{
	auto it = m_vpAudioClips.begin();

	while (it != m_vpAudioClips.end())
	{
		if ((*it) == p_pAudioClip) {
			delete *it;
			m_vpAudioClips.erase(it);
			return;
		}
		it++;
	}
}


void AudioManager::DeleteAudioMusic(Audio_Music* p_pAudioMusic)
{
	auto it = m_vpAudioMusic.begin();

	while (it != m_vpAudioMusic.end())
	{
		if ((*it) == p_pAudioMusic) {
			delete *it;
			m_vpAudioMusic.erase(it);
			return;
		}
		it++;
	}
}

