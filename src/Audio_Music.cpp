#include "stdafx.h"
#include "Audio_Music.h"


Audio_Music::Audio_Music(sf::Music* p_pMusic)
{
	m_pMusicStream = p_pMusic;
}


Audio_Music::~Audio_Music()
{

}


void Audio_Music::PlayMusic()
{
	m_pMusicStream->play();
}


void Audio_Music::PauseAudio()
{
	m_pMusicStream->pause();
}


void Audio_Music::StopAudio()
{
	m_pMusicStream->stop();
}


void Audio_Music::SetVolume(float p_Volume)
{
	m_pMusicStream->setVolume(MathEx::S_ClampValue(p_Volume, 0.0f, 100.0f));
}


void Audio_Music::SetPitch(float p_Pitch)
{
	m_pMusicStream->setPitch(p_Pitch);
}


AudioType Audio_Music::GetAudioType()
{
	return AudioType::AudioClipType_Music;
}


sf::SoundSource::Status Audio_Music::GetStatus()
{
	return m_pMusicStream->getStatus();
}


float Audio_Music::GetVolume()
{
	return m_pMusicStream->getVolume();
}


float Audio_Music::GetPitch()
{
	return m_pMusicStream->getVolume();
}

