#include "stdafx.h"
#include "LoadSpriteManager.h"


LoadSpriteManager::LoadSpriteManager()
{

}


LoadSpriteManager::~LoadSpriteManager()
{
	{
		auto it = m_vpSprites.begin();

		while (it != m_vpSprites.end())
		{
			delete (*it);
			it++;
		}
		m_vpSprites.clear();
	}
	{
		auto it = m_mpTextures.begin();

		while (it != m_mpTextures.end())
		{
			delete it->second;
			it++;
		}
		m_mpTextures.clear();
	}
}


sf::Sprite* LoadSpriteManager::CreateSprite(const std::string p_Filepath, sf::Vector2i p_Position, sf::Vector2i p_Dimension)
{
	auto it = m_mpTextures.find(p_Filepath);

	if (it == m_mpTextures.end())
	{
		sf::Texture* texture = new sf::Texture;
		texture->loadFromFile(p_Filepath);

		m_mpTextures.insert(std::pair<std::string, sf::Texture*>(p_Filepath, texture));
		it = m_mpTextures.find(p_Filepath);
	}

	sf::Sprite* sprite = new Sprite(*it->second, sf::IntRect{ p_Position, p_Dimension });
	m_vpSprites.push_back(sprite);

	return sprite;
}


sf::Sprite* LoadSpriteManager::CreateSprite(const std::string p_Filepath)
{
	auto it = m_mpTextures.find(p_Filepath);

	if (it == m_mpTextures.end())
	{
		sf::Texture* texture = new sf::Texture;
		texture->loadFromFile(p_Filepath);

		m_mpTextures.insert(std::pair<std::string, sf::Texture*>(p_Filepath, texture));
		it = m_mpTextures.find(p_Filepath);
	}

	sf::Sprite* sprite = new Sprite(*it->second);
	m_vpSprites.push_back(sprite);

	return sprite;
}


void LoadSpriteManager::DeleteSprite(sf::Sprite* p_pSprite)
{
	auto it = m_vpSprites.begin();

	while (it != m_vpSprites.end())
	{
		if ((*it) == p_pSprite) {
			delete (*it);
			m_vpSprites.erase(it);
			return;
		}
		it++;
	}
}

