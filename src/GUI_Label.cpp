#include "stdafx.h"
#include "GUI_Label.h"
#include "LoadSpriteManager.h"


GUI_Label::GUI_Label(sf::Vector2f p_Position, sf::Sprite* p_pSprite,
	LabelRenderData p_LabelRenderdata, LoadSpriteManager* p_pSpriteManager)
{
	m_pSprite = p_pSprite;
	m_pSprite->setPosition(p_Position);
	m_LabelRenderData = p_LabelRenderdata;
	m_pSpriteManager = p_pSpriteManager;
}


GUI_Label::~GUI_Label()
{
	m_pSpriteManager->DeleteSprite(m_pSprite);
	m_pSprite = nullptr;
}


void GUI_Label::OnContentChange(std::string p_NewContent)
{
	m_LabelRenderData.m_LabelText = p_NewContent;
}


GUI_Type GUI_Label::GetGUIType()
{
	return GUI_Type::GUI_Type_Label;
}


LabelRenderData* GUI_Label::GetLabelRenderData()
{
	return &m_LabelRenderData;
}


sf::Sprite* GUI_Label::GetSprite()
{
	return m_pSprite;
}


sf::Vector2f GUI_Label::GetPosition()
{
	return m_pSprite->getPosition();
}

