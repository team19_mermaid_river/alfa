#include "stdafx.h"
#include "GUI_Image.h"
#include "LoadSpriteManager.h"


GUI_Image::GUI_Image(sf::Sprite* p_pSprite, sf::Vector2f p_Position, LoadSpriteManager* p_pSpriteManager)
{
	m_pSprite = p_pSprite;
	m_pSprite->setPosition(p_Position);
	m_pSpriteManager = p_pSpriteManager;
}


GUI_Image::~GUI_Image()
{
	m_pSpriteManager->DeleteSprite(m_pSprite);
	m_pSprite = nullptr;
}


GUI_Type GUI_Image::GetGUIType()
{
	return GUI_Type::GUI_Type_Image;
}


sf::Sprite* GUI_Image::GetSprite()
{
	return m_pSprite;
}


sf::Vector2f GUI_Image::GetPosition()
{
	return m_pSprite->getPosition();
}

