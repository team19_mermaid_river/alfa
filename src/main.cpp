#include "stdafx.h"
#include "InputManager.h"
#include "MenuState.h"
#include "Background.h"

using namespace std;

MenuState* CreateMenu(Vector2f pos);
MenuState* TheMenu;
Background* CreateBackground(Vector2f p_Position);
Background* TheBackground;


int main(int argc, char** argv)
{
	RenderWindow* window = new RenderWindow(VideoMode(1920, 1080), "Mermaid River", sf::Style::Titlebar | sf::Style::Close);
	InputManager* inputManager = new InputManager(window);
	Clock* clock = new Clock();
	Time time;
	float deltaTime = 0;
	TheMenu = CreateMenu(Vector2f(0, 0));
	TheBackground = CreateBackground(Vector2f(0, 0));


	bool Quit = false;
	bool menuState = true;
	


	while (window->isOpen() && !Quit)
	{
		clock->restart();
		window->clear();


		time = clock->getElapsedTime();
		deltaTime = time.asSeconds();
		inputManager->Update(time);
		sf::IntRect rect = { 575,300,800,120 };

		if (inputManager->GetMousePosition().x > rect.left && inputManager->GetMousePosition().x < rect.left + rect.width)
		{
			if (inputManager->GetMousePosition().y > rect.top && inputManager->GetMousePosition().y < rect.top + rect.height)
			{
				if (inputManager->GetMouseButtonDown(0))
				{
					if (menuState == true)
					{
						menuState = false;
					}
				}
			}
		}
		if ((inputManager->GetKeyDown(sf::Keyboard::Escape)))
	{
			Quit = true;
		}
	
		if (menuState == true) {
			window->draw(*TheMenu->GetSprite());
		}
		if (menuState == false)
		{
			window->clear(sf::Color(0xff, 0xff, 0xff, 0xff));

			window->draw(*TheBackground->GetSprite());
		}
       window->display();
	}

	delete window;
	delete clock;

	getchar();
	
	return 0;
}


Background* CreateBackground(Vector2f p_Position)
{
	Texture* texture = new Texture();
	texture->loadFromFile("../assets/bakgrund.png", IntRect(0, 0, 1920, 1080));
	Sprite* sprite = new Sprite(*texture);

	Background* background = new Background(sprite, sf::Vector2f(p_Position.x, p_Position.y));
	return background;
}

MenuState* CreateMenu(Vector2f pos)
{
	sf::Texture* texture = new Texture();
	texture->loadFromFile("../assets/Menu_Pic.png", IntRect(0, 0, 1920, 1080));

	sf::Sprite* sprite = new Sprite(*texture);
	
	MenuState* menu = new MenuState(sprite, Vector2f(pos.x, pos.y));

	return menu;
}

