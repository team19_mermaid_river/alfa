#include "stdafx.h"
#include "GUI_Button.h"
#include "Collider_Box.h"
#include "Collider_Circle.h"
#include "CollisionManager.h"
#include "LoadSpriteManager.h"
#include "InputManager.h"



GUI_Button::GUI_Button(sf::Vector2f p_Position, sf::Sprite* p_pSprite,
	LabelRenderData p_LabelRenderdata, ButtonShape p_ButtonShape)
{
	m_pSprite = p_pSprite;
	m_pSprite->setPosition(p_Position);
	m_LabelRenderData = p_LabelRenderdata;

	if (p_ButtonShape == ButtonShape::ButtonShape_Box)
	{
		Collider_Box* collider = new Collider_Box(this);
		m_pCollider = dynamic_cast<Base_Collider*>(collider);
	}
	else if (p_ButtonShape == ButtonShape::ButtonShape_Circle) 
	{
		Collider_Circle* collider = new Collider_Circle(this);
		m_pCollider = dynamic_cast<Base_Collider*>(collider);
	}

	
}


GUI_Button::~GUI_Button()
{
	m_pSpriteManager->DeleteSprite(m_pSprite);
	m_pSprite = nullptr;
	delete m_pCollider;
	m_pCollider = nullptr;
}


bool GUI_Button::OnButtonPressed()
{
	if (m_pInputManager->GetMouseButtonDown(0))
	{
		if (m_pCollisionManager->InColliderBounds(m_pInputManager->GetMousePosition(), m_pCollider))
		{
			return true;
		}
	}
	return false;
}


GUI_Type GUI_Button::GetGUIType()
{
	return GUI_Type::GUI_Type_Button;
}


LabelRenderData* GUI_Button::GetLabelRenderData()
{
	return &m_LabelRenderData;
}


sf::Sprite* GUI_Button::GetSprite()
{
	return m_pSprite;
}


sf::Vector2f GUI_Button::GetPosition()
{
	return m_pSprite->getPosition();
}

