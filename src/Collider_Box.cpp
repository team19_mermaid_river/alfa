#include "stdafx.h"
#include "Collider_Box.h"
#include "PlayerEntity.h"
#include "Base_Pickup.h"
#include "Base_Entity.h"
#include "Base_Projectile.h"
#include "Base_GUI.h"


Collider_Box::Collider_Box(PlayerEntity* p_pParent)
{
	m_pPlayerParent = p_pParent;
	m_ParentType = ParentType::ParentType_Player;
}


Collider_Box::Collider_Box(Base_GUI* p_pParent)
{
	m_pGUIParent = p_pParent;
	m_ParentType = ParentType::ParentType_GUI;
}


Collider_Box::Collider_Box(Base_Entity* p_pParent)
{
	m_pEntityParent = p_pParent;
	m_ParentType = ParentType::ParentType_Entity;
}


Collider_Box::Collider_Box(Base_Pickup* p_pParent)
{
	m_pPickupParent = p_pParent;
	m_ParentType = ParentType::ParentType_Pickup;
}


Collider_Box::Collider_Box(Base_Projectile* p_pParent)
{
	m_pProjectileParent = p_pParent;
	m_ParentType = ParentType::ParentType_Projectile;
}


Collider_Box::~Collider_Box()
{

}


void Collider_Box::UpdateCollider()
{
	if (m_pEntityParent == nullptr && m_pPickupParent == nullptr && m_pPlayerParent == nullptr
		&& m_pProjectileParent == nullptr)
	{
		return;
	}

	switch (m_ParentType)
	{
	case ParentType::ParentType_Entity:
		if (m_pEntityParent != nullptr)
		{
			m_Dimension =
			{
				m_pEntityParent->GetSprite()->getLocalBounds().width,
				m_pEntityParent->GetSprite()->getLocalBounds().height
			};
			m_Position = m_pEntityParent->GetSprite()->getPosition();
		}
		break;
	case ParentType::ParentType_Pickup:
		if (m_pPickupParent != nullptr)
		{
			m_Dimension =
			{
				m_pPickupParent->GetSprite()->getLocalBounds().width,
				m_pPickupParent->GetSprite()->getLocalBounds().height
			};
			m_Position = m_pPickupParent->GetSprite()->getPosition();
		}
		break;
	case ParentType::ParentType_Player:
		if (m_pPlayerParent != nullptr)
		{
			m_Dimension =
			{
				m_pPlayerParent->GetSprite()->getLocalBounds().width,
				m_pPlayerParent->GetSprite()->getLocalBounds().height
			};
			m_Position = m_pPlayerParent->GetSprite()->getPosition();
		}
	case ParentType::ParentType_Projectile:
		if (m_pProjectileParent != nullptr)
		{
			m_Dimension =
			{
				m_pProjectileParent->GetSprite()->getLocalBounds().width,
				m_pProjectileParent->GetSprite()->getLocalBounds().height
			};
			m_Position = m_pProjectileParent->GetSprite()->getPosition();
		}
		break;
	case ParentType::ParentType_GUI:
		if (m_pGUIParent != nullptr)
		{
			m_Dimension =
			{
				m_pGUIParent->GetSprite()->getLocalBounds().width,
				m_pGUIParent->GetSprite()->getLocalBounds().height
			};
			m_Position = m_pGUIParent->GetSprite()->getPosition();
		}
		break;
	}
}


sf::Vector2f Collider_Box::GetPosition()
{
	return m_Position;
}


sf::Vector2f Collider_Box::GetDimension()
{
	return m_Dimension;
}


ColliderType Collider_Box::GetColliderType()
{
	return ColliderType::ColliderType_Box;
}

