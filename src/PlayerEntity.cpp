#include "stdafx.h"
#include "PlayerEntity.h"
#include "InputManager.h"
#include "LoadSpriteManager.h"
#include "Collider_Box.h"


PlayerEntity::PlayerEntity(sf::Vector2f p_Position, sf::Vector2f p_Speed)
{
	
	
	m_Speed = p_Speed;
	m_pSprite = m_pSpriteManager->CreateSprite("../assets/spritesheetswim.png", Vector2i(0, 0), Vector2i(293, 130));
	m_pSprite->setOrigin(Vector2f(m_pSprite->getLocalBounds().width * 0.5f, m_pSprite->getLocalBounds().height * 0.5f));
	m_pSprite->setPosition(p_Position);

	Collider_Box* box = new Collider_Box(this);
	m_pCollider = dynamic_cast<Base_Collider*>(box);
	m_pCollider->UpdateCollider();
}


PlayerEntity::~PlayerEntity()
{
	m_pSpriteManager->DeleteSprite(m_pSprite);
	m_pSprite = nullptr;
	delete m_pCollider;
	m_pCollider = nullptr;
}


void PlayerEntity::Update(float p_DeltaTime)
{
	if (m_pInputManager->GetAxis(InputAxis::InputAxis_Horizontal) != 0)
	{
		Vector2f position = m_pSprite->getPosition();
		position.x += m_pInputManager->GetAxis(InputAxis::InputAxis_Horizontal) * m_Speed.x * p_DeltaTime;
		m_pSprite->setPosition(position);
		std::cout << m_pSprite->getPosition().x << std::endl;
	}
	if (m_pInputManager->GetAxis(InputAxis::InputAxis_Vertical) != 0)
	{
		Vector2f position = m_pSprite->getPosition();
		position.y += m_pInputManager->GetAxis(InputAxis::InputAxis_Vertical) * m_Speed.y * p_DeltaTime;
		m_pSprite->setPosition(position);
		std::cout << m_pSprite->getPosition().y << std::endl;
	}
	ClampPlayerInScreen();
	m_pCollider->UpdateCollider();
}


void PlayerEntity::ClampPlayerInScreen()
{
	float halfHeight = m_pSprite->getGlobalBounds().height * 0.5f;
	float halfWidth = m_pSprite->getGlobalBounds().width * 0.5f;

	sf::Vector2f minPoint = sf::Vector2f(halfWidth, halfHeight);
	sf::Vector2f maxPoint = sf::Vector2f(m_ScreenDimension.x - halfWidth, m_ScreenDimension.y - halfHeight);

	m_pSprite->setPosition(MathEx::S_ClampVector(m_pSprite->getPosition(), minPoint, maxPoint));
}


sf::Sprite* PlayerEntity::GetSprite()
{
	return m_pSprite;
}


Base_Collider* PlayerEntity::GetCollider()
{
	return m_pCollider;
}


sf::Vector2f PlayerEntity::GetPosition()
{
	return m_pSprite->getPosition();
}

