#include "stdafx.h"
#include "Pickup_Coin.h"
#include "LoadSpriteManager.h"
#include "Collider_Circle.h"


Pickup_Coin::Pickup_Coin(sf::Vector2f p_Position, LoadSpriteManager* p_pSpriteManager)
{
	m_pSprite->setPosition(p_Position);
	m_pSpriteMananger = p_pSpriteManager;

	Collider_Circle* col = new Collider_Circle(this);
	m_pCollider = dynamic_cast<Base_Collider*>(col);
	m_pCollider->UpdateCollider();
}


Pickup_Coin::~Pickup_Coin()
{
	m_pSpriteMananger->DeleteSprite(m_pSprite);
	m_pSprite = nullptr;
	delete m_pCollider;
	m_pCollider = nullptr;
}

void Pickup_Coin::Update(float p_DeltaTime)
{
	m_pCollider->UpdateCollider();
}

Base_Collider* Pickup_Coin::GetCollider()
{
	return m_pCollider;
}

PickupType Pickup_Coin::GetPickupType()
{
	return PickupType::PickupType_Coin;
}

sf::Sprite * Pickup_Coin::GetSprite()
{
	return m_pSprite;
}

sf::Vector2f Pickup_Coin::GetPosition()
{
	return m_pSprite->getPosition();
}

