#include "stdafx.h"
#include "MenuState.h"


MenuState::MenuState(Sprite* p_pSprite, sf::Vector2f p_Position)
{
	m_Position = p_Position;
	m_pSprite = p_pSprite;
}


MenuState::~MenuState()
{
}

void MenuState::Update(float p_DeltaTime)
{
}

sf::Sprite* MenuState::GetSprite()
{
	return m_pSprite;
}

sf::Vector2f MenuState::GetPosition()
{
	return m_Position;
}
