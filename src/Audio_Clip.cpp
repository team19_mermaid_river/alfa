#include "stdafx.h"
#include "Audio_Clip.h"


Audio_Clip::Audio_Clip(sf::Sound* p_pSound)
{
	m_pSound = p_pSound;
}


Audio_Clip::~Audio_Clip()
{

}


void Audio_Clip::PlayOneShot()
{
	m_pSound->play();
}


void Audio_Clip::PauseAudio()
{
	m_pSound->pause();
}


void Audio_Clip::StopAudio()
{
	m_pSound->stop();
}


void Audio_Clip::SetVolume(float p_Volume)
{
	m_pSound->setVolume(MathEx::S_ClampValue(p_Volume, 0.0f, 100.0f));
}


void Audio_Clip::SetPitch(float p_Pitch)
{
	m_pSound->setPitch(p_Pitch);
}


AudioType Audio_Clip::GetAudioType()
{
	return AudioType::AudioClipType_Clip;
}


sf::SoundSource::Status Audio_Clip::GetStatus()
{
	return m_pSound->getStatus();
}


float Audio_Clip::GetVolume()
{
	return m_pSound->getVolume();
}


float Audio_Clip::GetPitch()
{
	return m_pSound->getVolume();
}