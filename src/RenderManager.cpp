#include "stdafx.h"
#include "RenderManager.h"


RenderManager::RenderManager()
{

}


RenderManager::~RenderManager()
{
	delete m_pWindow;
	m_pWindow = nullptr;
}


void RenderManager::IntializeRender(sf::Vector2i p_WindowDimension, const std::string p_WindowLabel, sf::Color p_RenderClearColor)
{
	sf::VideoMode videoMode = sf::VideoMode(m_WindowDimension.x, m_WindowDimension.y);
	m_pWindow = new sf::RenderWindow();
	m_pWindow->create(videoMode, p_WindowLabel, sf::Style::Titlebar | sf::Style::Close);
	m_RenderClearColor = p_RenderClearColor;
	m_pWindow->clear(m_RenderClearColor);
}


void RenderManager::RenderClear()
{
	m_pWindow->clear(m_RenderClearColor);
}


void RenderManager::RenderWindow(sf::Sprite* p_pSprite)
{
	m_pWindow->draw(*p_pSprite);
}


void RenderManager::RenderDisplay()
{
	m_pWindow->display();
}


sf::RenderWindow* RenderManager::GetWindow()
{
	return m_pWindow;
}


sf::Color RenderManager::GetRenderClearColor()
{
	return m_RenderClearColor;
}

