#include "stdafx.h"
#include "CollisionManager.h"
#include "Collider_Box.h"
#include "Collider_Circle.h"


CollisionManager::CollisionManager()
{

}


bool CollisionManager::OnCollision(Base_Collider* p_pCollider, Base_Collider* p_pColliderRef)
{
	bool collision = false;

	switch (p_pCollider->GetColliderType())
	{
	case ColliderType::ColliderType_Box:

		if (p_pColliderRef->GetColliderType() == ColliderType::ColliderType_Box)
		{
			collision = BoxOnBoxCollision(p_pCollider, p_pColliderRef);
		}
		else if (p_pColliderRef->GetColliderType() == ColliderType::ColliderType_Circle)
		{
			collision = BoxOnCircleCollision(p_pCollider, p_pColliderRef);
		}
		break;
	case ColliderType::ColliderType_Circle:

		if (p_pColliderRef->GetColliderType() == ColliderType::ColliderType_Box)
		{
			collision = BoxOnCircleCollision(p_pCollider, p_pColliderRef);
		}
		else if (p_pColliderRef->GetColliderType() == ColliderType::ColliderType_Circle)
		{
			collision = CircleOnCircleCollision(p_pCollider, p_pColliderRef);
		}
		break;
	}

	return collision;
}


bool CollisionManager::InColliderBounds(sf::Vector2i p_VectorPoint, Base_Collider* p_pCollider)
{
	bool inBounds = false;

	switch (p_pCollider->GetColliderType())
	{
	case ColliderType::ColliderType_Box:
		if (abs(p_pCollider->GetPosition().x - p_VectorPoint.x) <= p_pCollider->GetDimension().x / 2.0f)
		{
			if (abs(p_pCollider->GetPosition().y - p_VectorPoint.y) <= p_pCollider->GetDimension().y / 2.0f)
			{
				inBounds = true;
			}
		}
		break;
	case ColliderType::ColliderType_Circle:
		if (MathEx::S_VectorDistance(p_pCollider->GetPosition(), static_cast<sf::Vector2f>(p_VectorPoint)) < p_pCollider->GetRadius())
		{
			inBounds = true;
		}
		break;
	}

	return inBounds;
}


bool CollisionManager::BoxOnBoxCollision(Base_Collider* p_pCollider, Base_Collider* p_pColliderRef)
{
	Vector2f deltaCenter = p_pCollider->GetPosition() - p_pColliderRef->GetPosition();
	float deltaDimX = (p_pCollider->GetDimension().x / 2) + (p_pColliderRef->GetDimension().x / 2);
	float deltaDimY = (p_pCollider->GetDimension().y / 2) + (p_pColliderRef->GetDimension().y / 2);

	if (abs(deltaCenter.x) < deltaDimX)
	{
		if (abs(deltaCenter.y) < deltaDimY)
		{
			return true;
		}
	}
	return false;
}


bool CollisionManager::BoxOnCircleCollision(Base_Collider* p_pCollider, Base_Collider* p_pColliderRef)
{
	bool collision = false;
	sf::Vector2f deltaDistance = MathEx::S_VectorAbs(p_pCollider->GetPosition() - p_pColliderRef->GetPosition());

	if (deltaDistance.x <= (p_pColliderRef->GetDimension().x / 2))
	{
		collision = true;
	}
	else if (deltaDistance.y <= (p_pColliderRef->GetDimension().y / 2))
	{
		collision = true;
	}

	float cornerDistance = pow((deltaDistance.x - p_pColliderRef->GetDimension().x / 2), 2.0f) +
		pow((deltaDistance.y - p_pColliderRef->GetDimension().y / 2), 2.0f);

	collision = (cornerDistance <= pow(p_pCollider->GetRadius(), 2));

	return collision;
}


bool CollisionManager::CircleOnCircleCollision(Base_Collider* p_pCollider, Base_Collider* p_pColliderRef)
{
	float distance = MathEx::S_VectorDistance(p_pCollider->GetPosition(), p_pColliderRef->GetPosition());
	float deltaDistance = p_pCollider->GetRadius() + p_pColliderRef->GetRadius();

	return (distance <= deltaDistance);
}



