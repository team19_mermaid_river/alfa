#pragma once
#include "Base_Audio.h"


class Audio_Music : public Base_Audio
{

public:

	Audio_Music(sf::Music* p_pMusic);
	~Audio_Music();

	void PlayMusic();
	void PauseAudio();
	void StopAudio();

	void SetVolume(float p_Volume);
	void SetPitch(float p_Pitch);

	AudioType GetAudioType();
	sf::SoundSource::Status GetStatus();
	float GetVolume();
	float GetPitch();

private:

	sf::Music* m_pMusicStream;

};