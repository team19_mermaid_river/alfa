#pragma once

#include <SFML\Main.hpp>
#include <SFML\Audio.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\System.hpp>

#include <cmath>
#include <map>
#include <vector>
#include <iostream>

#include "MathExtension.h"