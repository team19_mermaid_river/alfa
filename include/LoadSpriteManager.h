#pragma once


class LoadSpriteManager
{

public:

	LoadSpriteManager();
	~LoadSpriteManager();

	sf::Sprite* CreateSprite(const std::string p_Filepath, sf::Vector2i p_Position, sf::Vector2i p_Dimension);
	sf::Sprite* CreateSprite(const std::string p_Filepath);

	void DeleteSprite(sf::Sprite* p_pSprite);

private:

	std::map<std::string, sf::Texture*> m_mpTextures;
	std::vector<sf::Sprite*> m_vpSprites;

};