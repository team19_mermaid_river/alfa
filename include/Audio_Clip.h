#pragma once
#include "Base_Audio.h"


class Audio_Clip
{

public:

	Audio_Clip(sf::Sound* p_pMusic);
	~Audio_Clip();

	void PlayOneShot();
	void PauseAudio();
	void StopAudio();

	void SetVolume(float p_Volume);
	void SetPitch(float p_Pitch);

	AudioType GetAudioType();
	sf::SoundSource::Status GetStatus();
	float GetVolume();
	float GetPitch();

private:

	sf::Sound* m_pSound;

};