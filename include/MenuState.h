#pragma once
class MenuState
{
public:
	MenuState(Sprite* p_pSprite, sf::Vector2f p_Position);
	~MenuState();

	void Update(float p_DeltaTime);
	sf::Sprite* GetSprite();
	sf::Vector2f GetPosition();
private:
	sf::Sprite* m_pSprite;
	sf::Vector2f m_Position;
};

