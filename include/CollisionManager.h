#pragma once

class Base_Collider;


class CollisionManager
{

public:

	CollisionManager();
	bool OnCollision(Base_Collider* p_pCollider, Base_Collider* p_pColliderRef);
	bool InColliderBounds(sf::Vector2i p_VectorPoint, Base_Collider* p_pCollider);

private:

	bool BoxOnBoxCollision(Base_Collider* p_pCollider, Base_Collider* p_pColliderRef);
	bool BoxOnCircleCollision(Base_Collider* p_pCollider, Base_Collider* p_pColliderRef);
	bool CircleOnCircleCollision(Base_Collider* p_pCollider, Base_Collider* p_pColliderRef);
};