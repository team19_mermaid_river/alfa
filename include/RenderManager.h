#pragma once


class RenderManager
{

	friend class Camera;

public:

	RenderManager();
	~RenderManager();

	void IntializeRender(sf::Vector2i p_WindowDimension,
		const std::string p_WindowLabel, sf::Color p_RenderClearColor);

	sf::RenderWindow* GetWindow();
	sf::Color GetRenderClearColor();

private:

	void RenderClear();
	void RenderWindow(sf::Sprite* p_pSprite);
	void RenderDisplay();

	sf::RenderWindow* m_pWindow;
	sf::Color m_RenderClearColor;
	sf::Vector2i m_WindowDimension;

};