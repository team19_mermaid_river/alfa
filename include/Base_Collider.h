#pragma once

class PlayerEntity;
class Base_Entity;
class Base_Pickup;
class Base_Projectile;
class Base_GUI;

enum ColliderType
{
	ColliderType_Box,
	ColliderType_Circle
};

enum ParentType
{
	ParentType_Entity,
	ParentType_GUI,
	ParentType_Player,
	ParentType_Pickup,
	ParentType_Projectile
};


class Base_Collider
{

public:

	virtual ~Base_Collider() {};
	virtual void UpdateCollider() = 0;
	virtual sf::Vector2f GetPosition() = 0;
	virtual sf::Vector2f GetDimension() { return Vector2f(); };
	virtual float GetRadius() { return 0.0f; };
	virtual ColliderType GetColliderType() = 0;

};