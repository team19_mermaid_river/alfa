#pragma once

class Background
{
public:
	Background(sf::Sprite* p_pSprite, sf::Vector2f p_Position);
	~Background();
	sf::Vector2f GetPosition();
	sf::Sprite* GetSprite();
private:
	sf::Vector2f m_Position;
	sf::Sprite* m_pSprite;
};

