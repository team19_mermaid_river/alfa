#pragma once
#include "Base_GUI.h"


class LoadSpriteManager;
class CollisionManager;
class InputManager;


class GUI_Button : public Base_GUI
{

public:

	GUI_Button(sf::Vector2f p_Position, sf::Sprite* p_pSprite,
		LabelRenderData p_LabelRenderdata, ButtonShape p_ButtonShape);
	~GUI_Button();

	bool OnButtonPressed();
	GUI_Type GetGUIType();
	LabelRenderData* GetLabelRenderData();
	sf::Sprite* GetSprite();
	sf::Vector2f GetPosition();

private:

	Base_Collider* m_pCollider;
	sf::Sprite* m_pSprite;
	LabelRenderData m_LabelRenderData;

	LoadSpriteManager* m_pSpriteManager;
	CollisionManager* m_pCollisionManager;
	InputManager* m_pInputManager;

};