#pragma once

enum AudioType
{
	AudioClipType_Music,
	AudioClipType_Clip
};


class Base_Audio
{

public:

	virtual ~Base_Audio() {};

	virtual void PlayMusic() {};
	virtual void PlayOneShot() {};
	virtual void PauseAudio() = 0;
	virtual void StopAudio() = 0;

	virtual void SetVolume(float p_Volume) = 0;
	virtual void SetPitch(float p_Pitch) = 0;

	virtual AudioType GetAudioType() = 0;
	virtual sf::SoundSource::Status GetStatus() = 0;
	virtual float GetVolume() = 0;
	virtual float GetPitch() = 0;

};