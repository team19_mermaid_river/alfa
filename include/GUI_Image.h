#pragma once
#include "Base_GUI.h"

class LoadSpriteManager;

class GUI_Image
{

public:

	GUI_Image(sf::Sprite* p_pSprite, sf::Vector2f p_Position, LoadSpriteManager* p_pSpriteManager);
	~GUI_Image();

	GUI_Type GetGUIType();
	sf::Sprite* GetSprite();
	sf::Vector2f GetPosition();

private:

	sf::Sprite* m_pSprite;
	LoadSpriteManager* m_pSpriteManager;

};