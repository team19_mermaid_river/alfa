#pragma once

class Base_Collider;
class LoadSpriteManager;

enum ProjectileType
{
	ProjectileType_CannonBall,
	ProjectileType_HarpoonSpear
};


class Base_Projectile
{

public:

	virtual ~Base_Projectile() {};

	virtual void Update(float p_DeltaTime) = 0;
	virtual ProjectileType GetProjectileType() = 0;
	virtual sf::Sprite* GetSprite() = 0;
	virtual sf::Vector2f GetPosition() = 0;
	virtual Base_Collider* GetCollider() = 0;

};