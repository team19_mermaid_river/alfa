#pragma once


class AudioManager;
class CollisionManager;
class InputManager;
class LoadSpriteManager;
class Base_Collider;
class Base_PowerUp;


class PlayerEntity
{

public:

	PlayerEntity(sf::Vector2f p_Position, sf::Vector2f p_Speed);
	~PlayerEntity();

	void Update(float p_DeltaTime);

	sf::Sprite* GetSprite();

	Base_Collider* GetCollider();
	sf::Vector2f GetPosition();

private:

	void ClampPlayerInScreen();

	sf::Vector2f m_Speed;
	sf::Vector2i m_ScreenDimension;
	sf::Sprite* m_pSprite;
	Base_Collider* m_pCollider;
	Base_PowerUp* m_pPowerUp;


	AudioManager* m_pAudioManager;
	CollisionManager* m_pCollisionManager;
	InputManager* m_pInputManager;
	LoadSpriteManager* m_pSpriteManager;

};