#pragma once

class Base_Projectile;
class PlayerEntity;

enum PowerupType
{
	PowerupType_Harpoon,
	PowerupType_Cannon,
	PowerupType_RumBottle
};


class Base_Powerup
{

public:

	virtual ~Base_Powerup() {};

	virtual void Update(float p_DeltaTime) = 0;
	virtual PowerupType GetPowerupType() = 0;
	virtual sf::Sprite* GetSprite() = 0;
	virtual sf::Vector2f GetPosition() = 0;
	virtual sf::Vector2f GetMuzzlePoint() { return sf::Vector2f(0.0f, 0.0f); };

};