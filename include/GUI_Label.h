#pragma once
#include "Base_GUI.h"

class LoadSpriteManager;


class GUI_Label : public Base_GUI
{

public:

	GUI_Label(sf::Vector2f p_Position, sf::Sprite* p_pSprite,
		LabelRenderData p_LabelRenderdata, LoadSpriteManager* p_pSpriteManager);
	~GUI_Label();

	void OnContentChange(std::string p_NewContent);
	GUI_Type GetGUIType();
	LabelRenderData* GetLabelRenderData();
	sf::Sprite* GetSprite();
	sf::Vector2f GetPosition();

private:

	sf::Sprite* m_pSprite;
	LabelRenderData m_LabelRenderData;

	LoadSpriteManager* m_pSpriteManager;

};