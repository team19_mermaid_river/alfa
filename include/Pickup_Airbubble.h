#pragma once
#include "Base_Pickup.h"


class Pickup_Airbubble : public Base_Pickup
{

public:

	Pickup_Airbubble(sf::Vector2f p_Position, int p_AirAmount, float p_RiseSpeed, LoadSpriteManager* p_pSpriteManager);
	~Pickup_Airbubble();

	void Update(float p_DeltaTime);
	Base_Collider* GetCollider();
	sf::Sprite* GetSprite();
	sf::Vector2f GetPosition();
	PickupType GetPickupType();
	int GetAirAmount();

private:

	sf::Vector2f Wobble(float p_DeltaTime);

	enum WobbleAxis {
		WobbleAxis_X,
		WobbleAxis_Y
	};

	const float m_WobbleSpeed = 1.5f;
	const float m_WobbleAmount = 0.25f;
	WobbleAxis m_WobbleAxis;
	float m_RiseSpeed;
	int m_AirAmount;

	LoadSpriteManager* m_pSpriteMananger;
	Base_Collider* m_pCollider;
	sf::Sprite* m_pSprite;
	sf::Vector2f m_Position;

};

