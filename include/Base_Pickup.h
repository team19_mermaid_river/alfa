#pragma once

class Base_Collider;
class LoadSpriteManager;

enum PickupType
{
	PickupType_Coin,
	PickupType_Airbubble
};


class Base_Pickup 
{
public:
	
	virtual ~Base_Pickup() {};
	virtual void Update(float p_DeltaTime) = 0;
	virtual sf::Sprite* GetSprite() = 0;
	virtual Base_Collider* GetCollider() = 0;
	virtual sf::Vector2f GetPosition() = 0;
	virtual int GetPoints() = 0;
	virtual PickupType GetPickupType() = 0;

};

