#pragma once

class Base_Collider;
class LoadSpriteManager;

enum EntityType
{
	EntityType_Swordfish,
	EntityType_Fangler
};


class Base_Entity
{

public:

	virtual ~Base_Entity() {};

	virtual void Update(float p_DeltaTime) = 0;
	virtual float GetImpactDamage() = 0;
	virtual sf::Sprite* GetSprite() = 0;
	virtual sf::Vector2f GetPosition() = 0;
	virtual Base_Collider* GetCollider() = 0;
	virtual EntityType GetEntityType() = 0;

};