#pragma once
#include "Base_GUI.h"

class LoadSpriteManager;


class GUI_Slider
{

public:

	GUI_Slider(sf::Vector2f p_Position, sf::Sprite* p_pSprite, sf::Vector2f p_SliderOffset,
		sf::Vector2f p_SliderDimension, sf::Color p_SliderColor,float p_MinValue, float p_MaxValue, float p_StartValue, LoadSpriteManager* p_pSpriteManager);
	~GUI_Slider();

	void OnValueChange(float p_Value);
	GUI_Type GetGUIType();
	SliderRenderData* GetSliderRenderData();
	sf::Vector2f GetPosition();

private:

	float m_MinValue, m_MaxValue, m_CurrentValue;
	sf::Vector2f m_OriginalRectSize;
	SliderRenderData m_SliderRenderData;
	LoadSpriteManager* m_pSpriteManager;

};