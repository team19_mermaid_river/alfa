#pragma once

class Base_Collider;

enum GUI_Type
{
	GUI_Type_Button,
	GUI_Type_Image,
	GUI_Type_Label,
	GUI_Type_Slider
};

enum ButtonShape
{
	ButtonShape_Box,
	ButtonShape_Circle
};

enum TextAlignment
{
	TextAlignment_Centered,
	TextAlignment_Left,
	TextAlignment_Right
};

struct LabelRenderData
{
	std::string m_LabelText;
	sf::Font* m_pFont;
	sf::Color m_TextColor;
	unsigned int m_CharacterSize;
	TextAlignment m_TextAlignment;
};

struct SliderRenderData
{
	sf::RectangleShape* m_pSliderRectangle;
	sf::Sprite* m_pBackgroundSprite;
};


class Base_GUI
{

public:

	virtual ~Base_GUI() {};

	virtual bool OnButtonPressed() { return false; };
	virtual void OnContentChange(std::string p_NewContent) {};
	virtual void OnValueChange(float p_Value) {};
	virtual GUI_Type GetGUIType() = 0;
	virtual LabelRenderData* GetLabelRenderData() { return nullptr; };
	virtual SliderRenderData* GetSliderRenderData() { return nullptr; };
	virtual sf::Sprite* GetSprite() { return nullptr; };
	virtual sf::Vector2f GetPosition() = 0;

};