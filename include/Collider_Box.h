#pragma once
#include "Base_Collider.h"


class Collider_Box : public Base_Collider
{

public:

	Collider_Box(PlayerEntity* p_pParent);
	Collider_Box(Base_GUI* p_pParent);
	Collider_Box(Base_Entity* p_pParent);
	Collider_Box(Base_Pickup* p_pParent);
	Collider_Box(Base_Projectile* p_pParent);
	~Collider_Box();

	void UpdateCollider();

	sf::Vector2f GetPosition();
	sf::Vector2f GetDimension();
	ColliderType GetColliderType();

private:

	sf::Vector2f m_Position;
	sf::Vector2f m_Dimension;

	PlayerEntity* m_pPlayerParent;
	Base_GUI* m_pGUIParent;
	Base_Entity* m_pEntityParent;
	Base_Pickup* m_pPickupParent;
	Base_Projectile* m_pProjectileParent;
	ParentType m_ParentType;

};