#pragma once

class Audio_Clip;
class Audio_Music;


class AudioManager
{

public:

	AudioManager();
	~AudioManager();

	Audio_Clip* CreateAudioClip(const std::string p_Filepath);
	Audio_Clip* CreateAudioClip(const std::string p_Filepath, float p_Volume, float p_Pitch);

	Audio_Music* CreateAudioMusic(const std::string p_Filepath);
	Audio_Music* CreateAudioMusic(const std::string p_Filepath, float p_Volume, float p_Pitch);

	void DeleteAudioClip(Audio_Clip* p_pAudioClip);
	void DeleteAudioMusic(Audio_Music* p_pAudioMusic);

private:

	std::map <std::string, sf::SoundBuffer*> m_mpSoundBuffers;
	std::vector <Audio_Clip*> m_vpAudioClips;
	std::vector <Audio_Music*> m_vpAudioMusic;

};