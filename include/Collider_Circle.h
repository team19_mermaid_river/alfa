#pragma once
#include "Base_Collider.h"


class Collider_Circle : public Base_Collider
{

public:

	Collider_Circle(PlayerEntity* p_pParent);
	Collider_Circle(Base_GUI* p_pParent);
	Collider_Circle(Base_Entity* p_pParent);
	Collider_Circle(Base_Pickup* p_pParent);
	Collider_Circle(Base_Projectile* p_pParent);
	~Collider_Circle();

	void UpdateCollider();

	sf::Vector2f GetPosition();
	float GetRadius();
	ColliderType GetColliderType();

private:

	float SetRadius(sf::Sprite* p_pSprite);

	sf::Vector2f m_Position;
	float m_Radius;

	PlayerEntity* m_pPlayerParent;
	Base_GUI* m_pGUIParent;
	Base_Entity* m_pEntityParent;
	Base_Pickup* m_pPickupParent;
	Base_Projectile* m_pProjectileParent;
	ParentType m_ParentType;

};