#pragma once
#include "Base_Pickup.h"


class Pickup_Coin : public Base_Pickup
{

public:

	Pickup_Coin(sf::Vector2f p_Position, LoadSpriteManager* p_pSpriteManager);
	~Pickup_Coin();

	void Update(float p_DeltaTime);
	Base_Collider* GetCollider();
	PickupType GetPickupType();
	sf::Sprite* GetSprite();
	sf::Vector2f GetPosition();

private:

	LoadSpriteManager* m_pSpriteMananger;
	PickupType m_PickupType;
	Base_Collider* m_pCollider;
	sf::Sprite* m_pSprite;
	
};

